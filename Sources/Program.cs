﻿using System;
using System.IO;
using System.Windows.Forms;
using youtube2itunes.Views;

namespace youtube2itunes
{
    static class Program
    {

        /// <summary>
        /// Point d'entrée
        /// </summary>
        [STAThread]
        static void Main()
         {
            if (String.IsNullOrEmpty(Properties.Settings.Default.TempDirectory))
                Properties.Settings.Default.TempDirectory = System.IO.Path.GetTempPath();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }

}