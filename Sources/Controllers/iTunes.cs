﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using iTunesLib;

using youtube2itunes.Controllers;

namespace youtube2itunes.Controllers
{

    [Serializable]
    /// <summary>
    /// Exception levée lorsqu'il est impossible de lancer iTunes.
    ///</summary>
    public class CouldNotStartiTunesException : Exception
    {
        public CouldNotStartiTunesException() : base("Impossible d'exécuter iTunes. L'application iTunes ne doit pas venir du Microsoft Store.") { }
    }

    [Serializable]
    /// <summary>
    /// Exception levée lorsqu'une opération avec iTunes a échoué
    ///</summary>
    public class OperationErroriTunesException : Exception
    {
        public IITObject ObjectThatCausedError { get; private set; }
        public Type ObjectType { get; private set; }

        public OperationErroriTunesException(IITObject objectThatCausedError, Type objectType) : base("Un composant iTunes a levé une exception. Traitement impossible")
        {
            ObjectThatCausedError = objectThatCausedError;
            ObjectType = objectType;
        }
    }

    [Serializable]
    /// <summary>
    /// Exception levée lorsqu'une opération sur une "source", donc un CD ou un iPod, a échoué
    ///</summary>
    public class OperationErrorSourceException : Exception
    {
        public IITSource DeviceSource { get; private set; }

        public OperationErrorSourceException(IITSource source) : base("Un périphérique lié à iTunes a levé une exception. Traitement impossible")
        {
            DeviceSource = source;
        }
    }


    #region "business object"
    /// <summary>
    /// 
    /// 
    /// Utilisable dans un BindingSource.
    /// </summary>
    public class iTunesPlayList : INotifyPropertyChanged
    {
        private IITPlaylist _playList;

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public string Name
        {
            get
            {
                return _playList.Name;
            }
            set
            {
                _playList.Name = value;
                _NotifyPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Constructeur adapté lorsque l'on récupère une playlist par programmation 
        /// 
        /// </summary>
        /// <param name="playList"></param>
        public iTunesPlayList(IITPlaylist playList)
        {
            _playList = playList;
        }

        /// <summary>
        /// Supprime la playlist et dispose de this en même temps
        /// </summary>
        public void Delete() => _playList.Delete();

        /// <summary>
        /// Propriété changée
        /// </summary>
        /// <param name="name"></param>
        private void _NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
    #endregion

    /// <summary>
    /// Contrôle iTunes afin de définir des playlists et synchroniser les musiques avec des périphériques
    /// </summary>
    class iTunesClient
    {
        private iTunesApp _iTunesApp;
        private IITSource _iPodDevice;

        /// <summary>
        /// Constructeur principal.
        /// 
        /// Ouvre iTunes.
        /// </summary>
        public iTunesClient()
        {
            _iTunesApp = new iTunesApp();
            if (_iTunesApp == null)
                throw new CouldNotStartiTunesException();

            _iTunesApp.BrowserWindow.Minimized = true;
        }

        /// <summary>
        /// Récupère les playlists iTunes
        /// </summary>
        /// <returns></returns>
        public List<iTunesPlayList> GetPlayLists()
        {
            var playLists = new List<iTunesPlayList>();
            foreach (IITPlaylist playList in _iTunesApp.LibrarySource.Playlists)
            {
                if (playList.Kind == ITPlaylistKind.ITPlaylistKindUser)
                {
                    var specialPlayList = (IITUserPlaylist) playList;
                    if (specialPlayList.SpecialKind == ITUserPlaylistSpecialKind.ITUserPlaylistSpecialKindNone)
                        playLists.Add(new iTunesPlayList(playList));
                }
            }

            return playLists;
        }

        /// <summary>
        /// Crée une nouvelle playlist
        /// </summary>
        /// <param name="name">Nom de la nouvelle playlist</param>
        /// <returns></returns>
        public iTunesPlayList CreateNewPlayList(string name)
        {
            return new iTunesPlayList(_iTunesApp.CreatePlaylist(name));
        }

        public void AddMusicToPlaylist(Medium medium, iTunesPlayList playList)
        {
            IITUserPlaylist userPlayList = (IITUserPlaylist)_iTunesApp.LibrarySource.Playlists.ItemByName[playList.Name];
            var result = userPlayList.AddFile(medium.FilePath);

            if (result == null)
                throw new OperationErroriTunesException(userPlayList, userPlayList.GetType());
        }

        /// <summary>
        /// Lance la synchronisation de l'iPod
        /// </summary>
        public void SynciPod()
        {
            try
            {
                _iTunesApp.UpdateIPod();
            }
            catch
            {
                throw new OperationErrorSourceException(_iPodDevice);
            }
        }

        /// <summary>
        /// Vérifie si un iPod est connecté
        /// </summary>
        /// <returns></returns>
        public bool IsiPodConnected()
        {
            var isConnected = false;
            foreach(IITSource source in _iTunesApp.Sources)
            {
                if (source.Kind == ITSourceKind.ITSourceKindIPod)
                {
                    isConnected = true;
                    _iPodDevice = source;
                    break;
                }
            }

            return isConnected;
        }

        /// <summary>
        /// Vérifie que iTunes soit toujours ouvert
        /// </summary>
        /// <returns></returns>
        public bool IsStillOpen()
        {
            return _iTunesApp != null;
        }
    }
}


