﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using YoutubeExplode;
using YoutubeExplode.Models.MediaStreams;

namespace youtube2itunes.Controllers
{
    [Serializable]
    /// <summary>
    /// Exception levée lors d'un erreur de téléchargement ou d'enregistrement de la vidéo.
    ///</summary>
    public class DownloadException : Exception
    {
        public Uri Url;
        public Medium Medium;

        public DownloadException(Uri url, Medium medium) : base($"Erreur lors du téléchargement de {medium.Title} de l'URL {url.ToString()}")
        {
            Url = url;
            Medium = medium;
        }
    }

    [Serializable]
    /// <summary>
    /// Exception levée lors d'une URL invalide 
    /// </summary>
    public class InvalidUrlException : Exception
    {
        public Uri Url;

        public InvalidUrlException(Uri url) : base($"URL {url.ToString()} invalide. Soyez sûr d'être sur une vidéo Youtube")
        {
            Url = url;
        }
    }

    /// <summary>
    /// Evènement levé lorsqu'un téléchargement vient de se terminer
    /// </summary>
    public class FinishedDownload: EventArgs
    {
        public readonly string Message;
        public readonly DateTime DateTime = DateTime.Now;
        public readonly bool HasFailed;
        public readonly bool HasBeenCancelled;

        public FinishedDownload(string message, bool hasFailed, bool hasBeenCancelled)
        {
            Message = message;
            HasFailed = hasFailed;
            HasBeenCancelled = hasBeenCancelled;
        }
    }

    #region "business object"
    /// <summary>
    /// Télécharge une vidéo Youtube.
    /// Il suffit ainsi de renseigner l'URL de la vidéo au constructeur
    /// et de démarrer le téléchargement avec Start().
    /// 
    /// On peut ainsi attendre la fin du téléchargement avec la méthode Wait() de l'objet Task retourné par Start()
    /// Ou écouter l'évènement DownloadFinished qui signale que le téléchargement s'est terminé.
    /// 
    /// Ensuite, il suffit de vérifier les propriétés HasFailed pour s'assurer que le téléchargement se soit bien déroulé
    /// (même si de toute façon des exceptions sont levées au cas où.
    /// 
    /// On accède ainsi à notre musique téléchargée via la propriété Medium.
    /// 
    /// Cet objet peut être parfaitement intégré à un BindingSource. 
    /// </summary>
    public class Download : INotifyPropertyChanged
    {
        private string _videoId;
        private string _downloadPath;
        private string _status;
        private string _title;

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        public event FinishedDownloadHandler DownloadFinished;
        public delegate void FinishedDownloadHandler(Download sender, FinishedDownload e);

        private Progress<double> _progress;
        private CancellationTokenSource _cancellationTokenSource;
        private Uri _url;
    
        private YoutubeClient _client;

        public bool IsFinished { get; private set; }
        public bool HasFailed { get; private set; }
        public bool HasBeenAborted { get; private set; }
        public Medium Medium;
        public string Status
        {
            get { return _status; }
            private set
            {
                _status = value;
                _NotifyPropertyChanged("Status");
            }
        }
        public string Title
        {
            get { return _title; }
            private set
            {
                _title = value;
                _NotifyPropertyChanged("Title");
            }
        }

        /// <summary>
        /// Constructeur principal
        /// </summary>
        /// <param name="url">URL de la vidéo Youtube</param>
        /// <param name="downloadPath">Répertoire où sera stockée la vidéo une fois téléchargée</param>
        public Download(Uri url, string downloadPath)
        {
            IsFinished = false;
            DownloadFinished += _OnFinishedDownload;

            if (!Directory.Exists(downloadPath))
                throw new IOException("Le répertoire de téléchargement n'existe pas");

            _url = url;
            _client = new YoutubeClient();

            if (!_IsUrlValid())
            {
                DownloadFinished(this, new FinishedDownload("Lien invalide", false, false));
                throw new InvalidUrlException(url);
            }

            _downloadPath = downloadPath;
   
            _progress = new Progress<double>(value => Status = $"{((int)(value * 100)).ToString()} %");

            Medium = new Medium();
        }

        /// <summary>
        /// S'assure que l'url mène bien vers une vidéo Youtube
        /// </summary>
        /// <returns>
        /// bool : Si oui ou non l'url est valide
        /// </returns>
        private bool _IsUrlValid()
        {
            if (!_url.AbsoluteUri.Contains("https://www.youtube.com/watch?v="))
                return false;

            var httpClient = new HttpClient();
            HttpResponseMessage response = httpClient.GetAsync(_url).Result;
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
                return false;

            return true;
        }

        /// <summary>
        /// Démarre le téléchargement
        /// </summary>
        /// <returns>Task exécutant le téléchargement</returns>
        public Task Start()
        {
            Status = "Démarrage...";

            _cancellationTokenSource = new CancellationTokenSource();
            return Task.Run(() =>
            {
                _DownloadVideo();
            }).ContinueWith((task) =>
            {
                _cancellationTokenSource.Dispose();

                if (! HasBeenAborted)
                {
                    if (task.IsFaulted)
                    {
                        DownloadFinished(this, new FinishedDownload("Échec", true, false));
                        throw new DownloadException(_url, Medium);
                    }
                    else
                        DownloadFinished(this, new FinishedDownload("Terminé", false, false));
                }
            });
        }

        private void _OnFinishedDownload(Download downloadedMusic, FinishedDownload finishedDownloadEvent)
        {
            Status = finishedDownloadEvent.Message;
            IsFinished = true;
            HasBeenAborted = finishedDownloadEvent.HasBeenCancelled;
            HasFailed = finishedDownloadEvent.HasFailed;
        }

        /// <summary>
        /// Arrête le téléchargement
        /// </summary>
        public void Cancel()
        {
            if (_cancellationTokenSource != null)
            {
                DownloadFinished(this, new FinishedDownload("Annulé", false, true));
                _cancellationTokenSource.Cancel();
             }
        }

        /// <summary>
        /// Récupère les informations d'une vidéo
        /// </summary>
        /// <returns>Informations sur la vidéo</returns>
        private YoutubeExplode.Models.Video _RetrieveVideoInfos()
        {
            bool isSuccessful = YoutubeClient.TryParseVideoId(_url.ToString(), out _videoId);

            if (!isSuccessful)
                throw new DownloadException(_url, new Medium());

            YoutubeExplode.Models.Video videoInfo = _client.GetVideoAsync(_videoId).Result;
            return videoInfo;
        }

        /// <summary>
        /// Télécharge une vidéo au format webm. Nécessite d'avoir récupéré les informations de la vidéo
        /// </summary>
        private void _DownloadVideo()
        {
            var videoInfo = _RetrieveVideoInfos();

            // Enlève tous les caractères invalides du titre (ceux non pris en charge par Windows)
            Title = String.Join("", videoInfo.Title.Split(Path.GetInvalidFileNameChars()));

            MediaStreamInfoSet streamInfoSet = _client.GetVideoMediaStreamInfosAsync(_videoId).Result;
            AudioStreamInfo audioStreamInfo = streamInfoSet.Audio.WithHighestBitrate();

            string fileExtension = audioStreamInfo.Container.GetFileExtension();
            string newMusicPath = Path.Combine(_downloadPath, $"{Title}.{fileExtension}");

            if (File.Exists(newMusicPath))
                File.Delete(newMusicPath);

            _client.DownloadMediaStreamAsync(audioStreamInfo, newMusicPath, _progress, _cancellationTokenSource.Token).Wait();

            Medium.FilePath = newMusicPath;
            Medium.Title = Title;
            Medium.Duration = videoInfo.Duration;
        }
        #endregion

        /// <summary>
        /// Propriété changée
        /// </summary>
        /// <param name="name"></param>
        private void _NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
