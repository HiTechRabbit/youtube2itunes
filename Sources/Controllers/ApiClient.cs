﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;

using DiscogsClient.Data.Query;
using DiscogsClient.Internal;

namespace youtube2itunes.Controllers
{

    [Serializable]
    public class NoNewResultFoundException : Exception
    {
        public readonly Medium Medium;

        public NoNewResultFoundException(Medium medium) : base($"Aucune information trouvée pour {medium.Title}")
        {
            Medium = medium;
        }
    }

    [Serializable]
    public class ApiAuthenticationException : Exception
    {
        public readonly string Token;

        public ApiAuthenticationException(string token) : base("Erreur d'authentification pour l'API musicale. Assurez-vous que la clef d'accès soit valide.")
        {
            Token = token;
        }
    }

    /// <summary>
    /// API simplifiée pour Discogs. 
    /// 
    /// Il suffit de renseigner au constructeur la clef d'accès du compte Discogs et 
    /// d'ensuite interroger l'API via GetMetaDataAsync(). La méthode retournera ainsi un objet Medium complété.
    /// </summary>
    public class ApiClient
    {
        private static readonly int MAX_RETURNED_RESULTS = 10;

        private DiscogsClient.DiscogsClient _discogsClient;

        /// <summary>
        /// Constructeur du client simplifié de l'API Discogs
        /// </summary>
        /// <param name="token">Token d'authentification. Peut-être généré depuis les paramètres du compte Discogs</param>
        public ApiClient(string token)
        {
            if (string.IsNullOrEmpty(token))
                throw new ApiAuthenticationException(token);

            var discogsToken = new TokenAuthenticationInformation(token);
            _discogsClient = new DiscogsClient.DiscogsClient(discogsToken);
        }

        /// <summary>
        /// Récupère les métadonnées de la musique depuis l'api Discogs
        /// </summary>
        /// <param name="medium">Musique à chercher</param>
        public async Task<Medium> GetMetaDataAsync(Medium medium)
        {
            var search = new DiscogsSearch()
            {
                query = medium.Title
            };

            if (! String.IsNullOrEmpty(medium.Artist))
            {
                search = new DiscogsSearch()
                {
                    release_title = medium.Title,
                    artist = medium.Artist
                };
            }

            DiscogsClient.Data.Result.DiscogsSearchResult appropriateResult = null;
            var results = _discogsClient.SearchAsEnumerable(search, MAX_RETURNED_RESULTS);
            foreach (var result in results)
            {
                if (result.type.Equals(DiscogsEntityType.release))
                {
                    appropriateResult = result;
                    break;
                }
            }

            if (appropriateResult == null)
                throw new NoNewResultFoundException(medium);

            var release = await _discogsClient.GetReleaseAsync(appropriateResult.id);

            medium.Artist = string.Join(" ", release.artists.Select(artist => artist.name).ToArray());
            medium.Genre = string.Join(" ", release.genres.Select(genre => genre.ToString()).ToArray());
            medium.Album = release.title;
            medium.Year = release.year;

            var communityRating = await _discogsClient.GetCommunityReleaseRatingAsync(release.id);
            medium.Notation = (int)communityRating.rating.average;

            var stream = new MemoryStream();
            await _discogsClient.DownloadImageAsync(release.images.First(), stream);

            medium.Image = Image.FromStream(stream);

            return medium;
        }
    }
}
