﻿using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Drawing2D;
using System;

namespace youtube2itunes.Controllers
{
    /// <summary>
    /// Fonctions d'aide 
    /// </summary>
    static class Helpers
    {
        public static Rectangle GetScaledRectangle(Image image, Rectangle thumbRectangle)
        {
            if (image.Width < thumbRectangle.Width && image.Height < thumbRectangle.Height)
                return new Rectangle(thumbRectangle.X + ((thumbRectangle.Width - image.Width) / 2), thumbRectangle.Y + ((thumbRectangle.Height - image.Height) / 2), image.Width, image.Height);

            var sourceWidth = image.Width;
            var sourceHeight = image.Height;

            float percent = 0;
            float percentWidth = 0;
            float percentHeight = 0;

            percentWidth = ((float)thumbRectangle.Width / (float)sourceWidth);
            percentHeight = ((float)thumbRectangle.Height / (float)sourceHeight);

            if (percentHeight < percentWidth)
                percent = percentHeight;
            else
                percent = percentWidth;

            var destWidth = (int)(sourceWidth * percent);
            var destHeight = (int)(sourceHeight * percent);

            if (destWidth.Equals(0))
                destWidth = 1;
            if (destHeight.Equals(0))
                destHeight = 1;

            var finalRectangle = new Rectangle(thumbRectangle.X, thumbRectangle.Y, destWidth, destHeight);

            if (finalRectangle.Height < thumbRectangle.Height)
                finalRectangle.Y = finalRectangle.Y + Convert.ToInt32(((float)thumbRectangle.Height - (float)finalRectangle.Height) / (float)2);

            if (finalRectangle.Width < thumbRectangle.Width)
                finalRectangle.X = finalRectangle.X + Convert.ToInt32(((float)thumbRectangle.Width - (float)finalRectangle.Width) / (float)2);

            return finalRectangle;
        }


        /// <summary>
        /// Adapte l'image à la taille du rectangle pour que ça fasse pas trop dégueu
        /// </summary>
        /// <param name="image">Image à réadapter</param>
        /// <param name="rectangle">Rectangle qui est censé accueillir l'image</param>
        /// <returns>Image réadaptée</returns>
        public static Image GetResizedImage(Image image, Rectangle rectangle)
        {
            var bitmap = new Bitmap(rectangle.Width, rectangle.Height);
            var graphic = Graphics.FromImage((Image)bitmap);
            graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphic.DrawImage(image, 0, 0, rectangle.Width, rectangle.Height);
            graphic.Dispose();

            try
            {
                return (Image)bitmap.Clone();
            }
            finally
            {
                bitmap.Dispose();
                bitmap = null;
                graphic = null;
            }
        }

    }
}
