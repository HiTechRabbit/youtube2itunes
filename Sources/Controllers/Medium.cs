﻿using System;
using System.Net.Mime;
using System.Drawing;
using System.IO;

using TagLib;

namespace youtube2itunes.Controllers
{
    #region "business object"
    /// <summary>
    /// Représentation d'une musique. 
    /// 
    /// Attention particularités !!!  
    /// Album(Medium) = Title(Discogs) = Titre de l'album ou de la collection
    /// Title(Medium) = Track(Discogs) = Titre de la musique
    /// </summary>
    public class Medium
    {
        public string FilePath { get; set; }
        public string Artist { get; set; } = "";
        public string Album { get; set; } = "";
        public string Title { get; set; } = "";
        public TimeSpan Duration { get; set; } = new TimeSpan(0);
        public string Comment { get; set; } = "";
        public string Genre { get; set; } = "";
        public int Year { get; set; } = 0;
        public int Notation { get; set; } = 0;
        public string Lyrics { get; set; } = "";
        public Image Image { get; set; }

        /// <summary>
        /// Enregistre le WebM en tant que MP3 dans un répertoire d'export
        /// </summary>
        /// <param name="tempDirectory">Dossier d'export temporaire</param>
        /// <param name="exportDirectory">Dossier d'export final</param>
        public void SaveAsMp3(string tempDirectory, string exportDirectory)
        {
            if (!Directory.Exists(exportDirectory) || !Directory.Exists(tempDirectory))
                throw new ArgumentException("Un des répertoires spécifié n'est pas accessible");

            var tempExportPath = Path.Combine(tempDirectory, $"{Path.GetFileNameWithoutExtension(FilePath)}.mp3");
            var finalExportPath = Path.Combine(exportDirectory, $"{Path.GetFileNameWithoutExtension(FilePath)}.mp3");

            cs_ffmpeg_mp3_converter.FFMpeg.Convert2Mp3(FilePath, tempExportPath);

            var file = TagLib.File.Create(tempExportPath);

            file.Tag.AlbumArtists = Artist.Split(',');
            file.Tag.Title = Title;
            file.Tag.Comment = Comment;
            file.Tag.Genres = Genre.Split(',');
            file.Tag.Album = Album;
            file.Tag.Year = (uint)Year;
            file.Tag.Lyrics = Lyrics;

            if (Image != null)
                _setImageToMP3(ref file);

            file.Save();

            System.IO.File.Move(tempExportPath, finalExportPath);
            FilePath = finalExportPath;
        }

        private void _setImageToMP3(ref TagLib.File file)
        {
            var pictureToSet = new Picture();

            pictureToSet.Type = PictureType.FrontCover;
            pictureToSet.Description = "Cover";
            pictureToSet.MimeType = MediaTypeNames.Image.Jpeg;

            var memoryStream = new MemoryStream();
            Image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            memoryStream.Position = 0;

            pictureToSet.Data = ByteVector.FromStream(memoryStream);
            file.Tag.Pictures = new IPicture[] { pictureToSet };

            memoryStream.Close();
        }
    }
    #endregion
}
