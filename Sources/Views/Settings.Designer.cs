﻿namespace youtube2itunes.Views
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.textBoxApiKey = new System.Windows.Forms.TextBox();
            this.buttonSetiTunesDirectory = new System.Windows.Forms.Button();
            this.buttonSetTempDirectory = new System.Windows.Forms.Button();
            this.textBoxiTunesDirectory = new System.Windows.Forms.TextBox();
            this.textBoxTempDirectory = new System.Windows.Forms.TextBox();
            this.labelApiKey = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelTempDirectory = new System.Windows.Forms.Label();
            this.panelMainControls = new System.Windows.Forms.Panel();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBoxSettings.SuspendLayout();
            this.panelMainControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.Controls.Add(this.textBoxApiKey);
            this.groupBoxSettings.Controls.Add(this.buttonSetiTunesDirectory);
            this.groupBoxSettings.Controls.Add(this.buttonSetTempDirectory);
            this.groupBoxSettings.Controls.Add(this.textBoxiTunesDirectory);
            this.groupBoxSettings.Controls.Add(this.textBoxTempDirectory);
            this.groupBoxSettings.Controls.Add(this.labelApiKey);
            this.groupBoxSettings.Controls.Add(this.label1);
            this.groupBoxSettings.Controls.Add(this.labelTempDirectory);
            this.groupBoxSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxSettings.Location = new System.Drawing.Point(0, 0);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(464, 281);
            this.groupBoxSettings.TabIndex = 0;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Paramètres principaux";
            // 
            // textBoxApiKey
            // 
            this.textBoxApiKey.Location = new System.Drawing.Point(162, 113);
            this.textBoxApiKey.Name = "textBoxApiKey";
            this.textBoxApiKey.Size = new System.Drawing.Size(295, 20);
            this.textBoxApiKey.TabIndex = 7;
            this.textBoxApiKey.Text = "WBASqnYgnKNEKCJtpuXXBBFWJjtmzSqRzNmbDsMs";
            // 
            // buttonSetiTunesDirectory
            // 
            this.buttonSetiTunesDirectory.Location = new System.Drawing.Point(433, 74);
            this.buttonSetiTunesDirectory.Name = "buttonSetiTunesDirectory";
            this.buttonSetiTunesDirectory.Size = new System.Drawing.Size(24, 24);
            this.buttonSetiTunesDirectory.TabIndex = 6;
            this.buttonSetiTunesDirectory.Text = "...";
            this.buttonSetiTunesDirectory.UseVisualStyleBackColor = true;
            this.buttonSetiTunesDirectory.Click += new System.EventHandler(this.OnButtonSetiTunesDirectory);
            // 
            // buttonSetTempDirectory
            // 
            this.buttonSetTempDirectory.Location = new System.Drawing.Point(433, 31);
            this.buttonSetTempDirectory.Name = "buttonSetTempDirectory";
            this.buttonSetTempDirectory.Size = new System.Drawing.Size(24, 24);
            this.buttonSetTempDirectory.TabIndex = 5;
            this.buttonSetTempDirectory.Text = "...";
            this.buttonSetTempDirectory.UseVisualStyleBackColor = true;
            this.buttonSetTempDirectory.Click += new System.EventHandler(this.OnButtonSetTempDirectoryClick);
            // 
            // textBoxiTunesDirectory
            // 
            this.textBoxiTunesDirectory.Location = new System.Drawing.Point(162, 76);
            this.textBoxiTunesDirectory.Name = "textBoxiTunesDirectory";
            this.textBoxiTunesDirectory.ReadOnly = true;
            this.textBoxiTunesDirectory.Size = new System.Drawing.Size(271, 20);
            this.textBoxiTunesDirectory.TabIndex = 4;
            this.textBoxiTunesDirectory.Text = "C:\\Users\\serva\\Desktop";
            // 
            // textBoxTempDirectory
            // 
            this.textBoxTempDirectory.Location = new System.Drawing.Point(162, 33);
            this.textBoxTempDirectory.Name = "textBoxTempDirectory";
            this.textBoxTempDirectory.ReadOnly = true;
            this.textBoxTempDirectory.Size = new System.Drawing.Size(271, 20);
            this.textBoxTempDirectory.TabIndex = 3;
            this.textBoxTempDirectory.Text = "C:\\Users\\serva\\Desktop";
            // 
            // labelApiKey
            // 
            this.labelApiKey.AutoSize = true;
            this.labelApiKey.Location = new System.Drawing.Point(22, 116);
            this.labelApiKey.Name = "labelApiKey";
            this.labelApiKey.Size = new System.Drawing.Size(72, 13);
            this.labelApiKey.TabIndex = 2;
            this.labelApiKey.Text = "Clef Discogs :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Répertoire d\'export iTunes :";
            // 
            // labelTempDirectory
            // 
            this.labelTempDirectory.AutoSize = true;
            this.labelTempDirectory.Location = new System.Drawing.Point(22, 36);
            this.labelTempDirectory.Name = "labelTempDirectory";
            this.labelTempDirectory.Size = new System.Drawing.Size(114, 13);
            this.labelTempDirectory.TabIndex = 0;
            this.labelTempDirectory.Text = "Répertoire temporaire :";
            // 
            // panelMainControls
            // 
            this.panelMainControls.AllowDrop = true;
            this.panelMainControls.BackColor = System.Drawing.SystemColors.Control;
            this.panelMainControls.Controls.Add(this.buttonOK);
            this.panelMainControls.Controls.Add(this.buttonCancel);
            this.panelMainControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelMainControls.Location = new System.Drawing.Point(0, 242);
            this.panelMainControls.Name = "panelMainControls";
            this.panelMainControls.Size = new System.Drawing.Size(464, 39);
            this.panelMainControls.TabIndex = 1;
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonOK.Location = new System.Drawing.Point(302, 10);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 3;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.OnButtonOKClick);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(383, 10);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Annuler";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.OnButtonCancelClick);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(464, 281);
            this.Controls.Add(this.panelMainControls);
            this.Controls.Add(this.groupBoxSettings);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Settings";
            this.Text = "Paramètres";
            this.groupBoxSettings.ResumeLayout(false);
            this.groupBoxSettings.PerformLayout();
            this.panelMainControls.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.Panel panelMainControls;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBoxTempDirectory;
        private System.Windows.Forms.Label labelApiKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelTempDirectory;
        private System.Windows.Forms.Button buttonSetiTunesDirectory;
        private System.Windows.Forms.Button buttonSetTempDirectory;
        private System.Windows.Forms.TextBox textBoxiTunesDirectory;
        private System.Windows.Forms.TextBox textBoxApiKey;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    }
}