﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace youtube2itunes.Views
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();

            textBoxTempDirectory.Text = Properties.Settings.Default.TempDirectory;
            textBoxiTunesDirectory.Text = Properties.Settings.Default.iTunesMusicDirectory;
            textBoxApiKey.Text = Properties.Settings.Default.ApiKey;
        }

        private void OnButtonOKClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;

            Properties.Settings.Default.TempDirectory = textBoxTempDirectory.Text;
            Properties.Settings.Default.iTunesMusicDirectory = textBoxiTunesDirectory.Text;
            Properties.Settings.Default.ApiKey = textBoxApiKey.Text;

            Properties.Settings.Default.Save();

            Close();
        }

        private void OnButtonSetTempDirectoryClick(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
                textBoxTempDirectory.Text = folderBrowserDialog.SelectedPath;
        }

        private void OnButtonSetiTunesDirectory(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
                textBoxiTunesDirectory.Text = folderBrowserDialog.SelectedPath;
        }

        private void OnButtonCancelClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
