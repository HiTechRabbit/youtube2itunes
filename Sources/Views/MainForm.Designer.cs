﻿namespace youtube2itunes.Views
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelMainControls = new System.Windows.Forms.Panel();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrevious = new System.Windows.Forms.Button();
            this.buttonQuit = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabYoutube = new System.Windows.Forms.TabPage();
            this.webBrowser = new Gecko.GeckoWebBrowser();
            this.panelWebBrowserControls = new System.Windows.Forms.Panel();
            this.textBoxURL = new System.Windows.Forms.TextBox();
            this.buttonForward = new System.Windows.Forms.Button();
            this.buttonBack = new System.Windows.Forms.Button();
            this.buttonHome = new System.Windows.Forms.Button();
            this.buttonDownload = new System.Windows.Forms.Button();
            this.groupBoxDownloads = new System.Windows.Forms.GroupBox();
            this.labelTempDirectoryText = new System.Windows.Forms.Label();
            this.dataGridViewDownloads = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumnTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumnStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceDownloads = new System.Windows.Forms.BindingSource(this.components);
            this.buttonCancelDownload = new System.Windows.Forms.Button();
            this.labelTempDirectory = new System.Windows.Forms.Label();
            this.tabMusics = new System.Windows.Forms.TabPage();
            this.groupBoxMusicEdit = new System.Windows.Forms.GroupBox();
            this.labelProcessedMedium = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.buttonSaveMusics = new System.Windows.Forms.Button();
            this.numericUpDownYear = new System.Windows.Forms.NumericUpDown();
            this.bindingSourceMedia = new System.Windows.Forms.BindingSource(this.components);
            this.numericUpDownNotation = new System.Windows.Forms.NumericUpDown();
            this.buttonLookOnTheInternet = new System.Windows.Forms.Button();
            this.labelNextDirectory = new System.Windows.Forms.Label();
            this.textBoxNextDirectory = new System.Windows.Forms.TextBox();
            this.buttonCancelMusic = new System.Windows.Forms.Button();
            this.buttonApplyMusic = new System.Windows.Forms.Button();
            this.labelLyrics = new System.Windows.Forms.Label();
            this.textBoxLyrics = new System.Windows.Forms.TextBox();
            this.labelYear = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.labelGrade = new System.Windows.Forms.Label();
            this.labelLength = new System.Windows.Forms.Label();
            this.textBoxLength = new System.Windows.Forms.TextBox();
            this.labelGenre = new System.Windows.Forms.Label();
            this.textBoxGenre = new System.Windows.Forms.TextBox();
            this.labelArtist = new System.Windows.Forms.Label();
            this.textBoxArtist = new System.Windows.Forms.TextBox();
            this.labelAlbum = new System.Windows.Forms.Label();
            this.textBoxAlbum = new System.Windows.Forms.TextBox();
            this.labelTitle = new System.Windows.Forms.Label();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.groupBoxMusics = new System.Windows.Forms.GroupBox();
            this.dataGridViewMedia = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumnMediumTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelTempDirectory2Text = new System.Windows.Forms.Label();
            this.labelTempDirectory2 = new System.Windows.Forms.Label();
            this.tabSynchronize = new System.Windows.Forms.TabPage();
            this.groupBoxSynchronize = new System.Windows.Forms.GroupBox();
            this.buttonDetectiPod = new System.Windows.Forms.Button();
            this.buttonSync = new System.Windows.Forms.Button();
            this.buttonAddMusicToPlaylist = new System.Windows.Forms.Button();
            this.buttonRemoveMusicFromPlaylist = new System.Windows.Forms.Button();
            this.buttonDeletePlayList = new System.Windows.Forms.Button();
            this.buttonAddPlayList = new System.Windows.Forms.Button();
            this.textBoxPlayList = new System.Windows.Forms.TextBox();
            this.treeView = new System.Windows.Forms.TreeView();
            this.labelIpodConnectionStatusText = new System.Windows.Forms.Label();
            this.labelItunesStatusText = new System.Windows.Forms.Label();
            this.labelIsIpodConnected = new System.Windows.Forms.Label();
            this.labelIsItunesAvalaible = new System.Windows.Forms.Label();
            this.dataGridViewMusicsToSynchronize = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Artist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceMediaToSync = new System.Windows.Forms.BindingSource(this.components);
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemQuit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.panelMainControls.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabYoutube.SuspendLayout();
            this.panelWebBrowserControls.SuspendLayout();
            this.groupBoxDownloads.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDownloads)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDownloads)).BeginInit();
            this.tabMusics.SuspendLayout();
            this.groupBoxMusicEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceMedia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNotation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.groupBoxMusics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMedia)).BeginInit();
            this.tabSynchronize.SuspendLayout();
            this.groupBoxSynchronize.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMusicsToSynchronize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceMediaToSync)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMainControls
            // 
            this.panelMainControls.Controls.Add(this.buttonNext);
            this.panelMainControls.Controls.Add(this.buttonPrevious);
            this.panelMainControls.Controls.Add(this.buttonQuit);
            this.panelMainControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelMainControls.Location = new System.Drawing.Point(0, 690);
            this.panelMainControls.Name = "panelMainControls";
            this.panelMainControls.Size = new System.Drawing.Size(1008, 40);
            this.panelMainControls.TabIndex = 2;
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNext.Image = global::youtube2itunes.Properties.Resources.smallforward;
            this.buttonNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonNext.Location = new System.Drawing.Point(918, 6);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(83, 23);
            this.buttonNext.TabIndex = 3;
            this.buttonNext.Text = "Suivant";
            this.buttonNext.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.OnButtonNextClick);
            // 
            // buttonPrevious
            // 
            this.buttonPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrevious.Enabled = false;
            this.buttonPrevious.Image = global::youtube2itunes.Properties.Resources.smallback;
            this.buttonPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPrevious.Location = new System.Drawing.Point(829, 6);
            this.buttonPrevious.Name = "buttonPrevious";
            this.buttonPrevious.Size = new System.Drawing.Size(83, 23);
            this.buttonPrevious.TabIndex = 2;
            this.buttonPrevious.Text = "Précédent";
            this.buttonPrevious.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonPrevious.UseVisualStyleBackColor = true;
            this.buttonPrevious.Click += new System.EventHandler(this.OnButtonPreviousClick);
            // 
            // buttonQuit
            // 
            this.buttonQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonQuit.Location = new System.Drawing.Point(12, 6);
            this.buttonQuit.Name = "buttonQuit";
            this.buttonQuit.Size = new System.Drawing.Size(75, 23);
            this.buttonQuit.TabIndex = 1;
            this.buttonQuit.Text = "Quitter";
            this.buttonQuit.UseVisualStyleBackColor = true;
            this.buttonQuit.Click += new System.EventHandler(this.OnButtonCancelClick);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabYoutube);
            this.tabControl.Controls.Add(this.tabMusics);
            this.tabControl.Controls.Add(this.tabSynchronize);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 24);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1008, 666);
            this.tabControl.TabIndex = 3;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.OnTabControlSelectionIndexChanged);
            // 
            // tabYoutube
            // 
            this.tabYoutube.Controls.Add(this.webBrowser);
            this.tabYoutube.Controls.Add(this.panelWebBrowserControls);
            this.tabYoutube.Controls.Add(this.groupBoxDownloads);
            this.tabYoutube.Location = new System.Drawing.Point(4, 22);
            this.tabYoutube.Name = "tabYoutube";
            this.tabYoutube.Padding = new System.Windows.Forms.Padding(3);
            this.tabYoutube.Size = new System.Drawing.Size(1000, 640);
            this.tabYoutube.TabIndex = 0;
            this.tabYoutube.Text = "Télécharger depuis Youtube";
            this.tabYoutube.UseVisualStyleBackColor = true;
            // 
            // webBrowser
            // 
            this.webBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser.FrameEventsPropagateToMainWindow = false;
            this.webBrowser.Location = new System.Drawing.Point(3, 57);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(714, 574);
            this.webBrowser.TabIndex = 2;
            this.webBrowser.UseHttpActivityObserver = false;
            this.webBrowser.UseWaitCursor = true;
            this.webBrowser.Navigated += new System.EventHandler<Gecko.GeckoNavigatedEventArgs>(this.OnWebBrowserNavigated);
            // 
            // panelWebBrowserControls
            // 
            this.panelWebBrowserControls.Controls.Add(this.textBoxURL);
            this.panelWebBrowserControls.Controls.Add(this.buttonForward);
            this.panelWebBrowserControls.Controls.Add(this.buttonBack);
            this.panelWebBrowserControls.Controls.Add(this.buttonHome);
            this.panelWebBrowserControls.Controls.Add(this.buttonDownload);
            this.panelWebBrowserControls.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelWebBrowserControls.Location = new System.Drawing.Point(3, 3);
            this.panelWebBrowserControls.Name = "panelWebBrowserControls";
            this.panelWebBrowserControls.Size = new System.Drawing.Size(720, 48);
            this.panelWebBrowserControls.TabIndex = 1;
            // 
            // textBoxURL
            // 
            this.textBoxURL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxURL.Location = new System.Drawing.Point(142, 6);
            this.textBoxURL.Name = "textBoxURL";
            this.textBoxURL.ReadOnly = true;
            this.textBoxURL.Size = new System.Drawing.Size(525, 35);
            this.textBoxURL.TabIndex = 4;
            // 
            // buttonForward
            // 
            this.buttonForward.Image = global::youtube2itunes.Properties.Resources.forward;
            this.buttonForward.Location = new System.Drawing.Point(97, 3);
            this.buttonForward.Name = "buttonForward";
            this.buttonForward.Size = new System.Drawing.Size(40, 40);
            this.buttonForward.TabIndex = 3;
            this.buttonForward.UseVisualStyleBackColor = true;
            this.buttonForward.Click += new System.EventHandler(this.OnButtonForwardClick);
            // 
            // buttonBack
            // 
            this.buttonBack.Image = global::youtube2itunes.Properties.Resources.back;
            this.buttonBack.Location = new System.Drawing.Point(51, 3);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(40, 40);
            this.buttonBack.TabIndex = 2;
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.OnButtonBackClick);
            // 
            // buttonHome
            // 
            this.buttonHome.Image = global::youtube2itunes.Properties.Resources.home;
            this.buttonHome.Location = new System.Drawing.Point(5, 3);
            this.buttonHome.Name = "buttonHome";
            this.buttonHome.Size = new System.Drawing.Size(40, 40);
            this.buttonHome.TabIndex = 1;
            this.buttonHome.UseVisualStyleBackColor = true;
            this.buttonHome.Click += new System.EventHandler(this.OnButtonHomeClick);
            // 
            // buttonDownload
            // 
            this.buttonDownload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDownload.Enabled = false;
            this.buttonDownload.Image = global::youtube2itunes.Properties.Resources.download;
            this.buttonDownload.Location = new System.Drawing.Point(674, 3);
            this.buttonDownload.Name = "buttonDownload";
            this.buttonDownload.Size = new System.Drawing.Size(40, 40);
            this.buttonDownload.TabIndex = 0;
            this.buttonDownload.UseVisualStyleBackColor = true;
            this.buttonDownload.Click += new System.EventHandler(this.OnButtonDownloadClick);
            // 
            // groupBoxDownloads
            // 
            this.groupBoxDownloads.Controls.Add(this.labelTempDirectoryText);
            this.groupBoxDownloads.Controls.Add(this.dataGridViewDownloads);
            this.groupBoxDownloads.Controls.Add(this.buttonCancelDownload);
            this.groupBoxDownloads.Controls.Add(this.labelTempDirectory);
            this.groupBoxDownloads.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBoxDownloads.Location = new System.Drawing.Point(723, 3);
            this.groupBoxDownloads.Name = "groupBoxDownloads";
            this.groupBoxDownloads.Size = new System.Drawing.Size(274, 634);
            this.groupBoxDownloads.TabIndex = 0;
            this.groupBoxDownloads.TabStop = false;
            this.groupBoxDownloads.Text = "Opérations";
            // 
            // labelTempDirectoryText
            // 
            this.labelTempDirectoryText.AutoSize = true;
            this.labelTempDirectoryText.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::youtube2itunes.Properties.Settings.Default, "TempDirectory", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labelTempDirectoryText.Location = new System.Drawing.Point(6, 30);
            this.labelTempDirectoryText.Name = "labelTempDirectoryText";
            this.labelTempDirectoryText.Size = new System.Drawing.Size(0, 13);
            this.labelTempDirectoryText.TabIndex = 5;
            this.labelTempDirectoryText.Text = global::youtube2itunes.Properties.Settings.Default.TempDirectory;
            // 
            // dataGridViewDownloads
            // 
            this.dataGridViewDownloads.AllowUserToAddRows = false;
            this.dataGridViewDownloads.AllowUserToDeleteRows = false;
            this.dataGridViewDownloads.AllowUserToResizeColumns = false;
            this.dataGridViewDownloads.AllowUserToResizeRows = false;
            this.dataGridViewDownloads.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridViewDownloads.AutoGenerateColumns = false;
            this.dataGridViewDownloads.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDownloads.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumnTitle,
            this.dataGridViewTextBoxColumnStatus});
            this.dataGridViewDownloads.DataSource = this.bindingSourceDownloads;
            this.dataGridViewDownloads.Location = new System.Drawing.Point(6, 54);
            this.dataGridViewDownloads.MultiSelect = false;
            this.dataGridViewDownloads.Name = "dataGridViewDownloads";
            this.dataGridViewDownloads.RowHeadersVisible = false;
            this.dataGridViewDownloads.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewDownloads.Size = new System.Drawing.Size(265, 548);
            this.dataGridViewDownloads.TabIndex = 4;
            this.dataGridViewDownloads.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.OnDataGridViewDownloadsRowsAdded);
            this.dataGridViewDownloads.SelectionChanged += new System.EventHandler(this.OnDataGridViewDownloadsSelectionChanged);
            // 
            // dataGridViewTextBoxColumnTitle
            // 
            this.dataGridViewTextBoxColumnTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumnTitle.DataPropertyName = "Title";
            this.dataGridViewTextBoxColumnTitle.HeaderText = "Titre";
            this.dataGridViewTextBoxColumnTitle.MinimumWidth = 150;
            this.dataGridViewTextBoxColumnTitle.Name = "dataGridViewTextBoxColumnTitle";
            this.dataGridViewTextBoxColumnTitle.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumnStatus
            // 
            this.dataGridViewTextBoxColumnStatus.DataPropertyName = "Status";
            this.dataGridViewTextBoxColumnStatus.HeaderText = "Statut";
            this.dataGridViewTextBoxColumnStatus.Name = "dataGridViewTextBoxColumnStatus";
            this.dataGridViewTextBoxColumnStatus.ReadOnly = true;
            this.dataGridViewTextBoxColumnStatus.Width = 80;
            // 
            // bindingSourceDownloads
            // 
            this.bindingSourceDownloads.AllowNew = false;
            this.bindingSourceDownloads.DataSource = typeof(youtube2itunes.Controllers.Download);
            // 
            // buttonCancelDownload
            // 
            this.buttonCancelDownload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCancelDownload.Enabled = false;
            this.buttonCancelDownload.Location = new System.Drawing.Point(6, 605);
            this.buttonCancelDownload.Name = "buttonCancelDownload";
            this.buttonCancelDownload.Size = new System.Drawing.Size(75, 23);
            this.buttonCancelDownload.TabIndex = 3;
            this.buttonCancelDownload.Text = "Annuler";
            this.buttonCancelDownload.UseVisualStyleBackColor = true;
            this.buttonCancelDownload.Click += new System.EventHandler(this.OnButtonCancelDownloadClick);
            // 
            // labelTempDirectory
            // 
            this.labelTempDirectory.AutoSize = true;
            this.labelTempDirectory.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelTempDirectory.Location = new System.Drawing.Point(6, 16);
            this.labelTempDirectory.Name = "labelTempDirectory";
            this.labelTempDirectory.Size = new System.Drawing.Size(117, 13);
            this.labelTempDirectory.TabIndex = 1;
            this.labelTempDirectory.Text = "Répertoire temporaire : ";
            // 
            // tabMusics
            // 
            this.tabMusics.Controls.Add(this.groupBoxMusicEdit);
            this.tabMusics.Controls.Add(this.groupBoxMusics);
            this.tabMusics.Location = new System.Drawing.Point(4, 22);
            this.tabMusics.Name = "tabMusics";
            this.tabMusics.Padding = new System.Windows.Forms.Padding(3);
            this.tabMusics.Size = new System.Drawing.Size(1000, 640);
            this.tabMusics.TabIndex = 1;
            this.tabMusics.Text = "Enregistrer les musiques";
            this.tabMusics.UseVisualStyleBackColor = true;
            // 
            // groupBoxMusicEdit
            // 
            this.groupBoxMusicEdit.Controls.Add(this.labelProcessedMedium);
            this.groupBoxMusicEdit.Controls.Add(this.progressBar);
            this.groupBoxMusicEdit.Controls.Add(this.buttonSaveMusics);
            this.groupBoxMusicEdit.Controls.Add(this.numericUpDownYear);
            this.groupBoxMusicEdit.Controls.Add(this.numericUpDownNotation);
            this.groupBoxMusicEdit.Controls.Add(this.buttonLookOnTheInternet);
            this.groupBoxMusicEdit.Controls.Add(this.labelNextDirectory);
            this.groupBoxMusicEdit.Controls.Add(this.textBoxNextDirectory);
            this.groupBoxMusicEdit.Controls.Add(this.buttonCancelMusic);
            this.groupBoxMusicEdit.Controls.Add(this.buttonApplyMusic);
            this.groupBoxMusicEdit.Controls.Add(this.labelLyrics);
            this.groupBoxMusicEdit.Controls.Add(this.textBoxLyrics);
            this.groupBoxMusicEdit.Controls.Add(this.labelYear);
            this.groupBoxMusicEdit.Controls.Add(this.pictureBox);
            this.groupBoxMusicEdit.Controls.Add(this.labelGrade);
            this.groupBoxMusicEdit.Controls.Add(this.labelLength);
            this.groupBoxMusicEdit.Controls.Add(this.textBoxLength);
            this.groupBoxMusicEdit.Controls.Add(this.labelGenre);
            this.groupBoxMusicEdit.Controls.Add(this.textBoxGenre);
            this.groupBoxMusicEdit.Controls.Add(this.labelArtist);
            this.groupBoxMusicEdit.Controls.Add(this.textBoxArtist);
            this.groupBoxMusicEdit.Controls.Add(this.labelAlbum);
            this.groupBoxMusicEdit.Controls.Add(this.textBoxAlbum);
            this.groupBoxMusicEdit.Controls.Add(this.labelTitle);
            this.groupBoxMusicEdit.Controls.Add(this.textBoxTitle);
            this.groupBoxMusicEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxMusicEdit.Location = new System.Drawing.Point(277, 3);
            this.groupBoxMusicEdit.Name = "groupBoxMusicEdit";
            this.groupBoxMusicEdit.Size = new System.Drawing.Size(720, 634);
            this.groupBoxMusicEdit.TabIndex = 2;
            this.groupBoxMusicEdit.TabStop = false;
            this.groupBoxMusicEdit.Text = "Musique sélectionnée";
            // 
            // labelProcessedMedium
            // 
            this.labelProcessedMedium.AutoSize = true;
            this.labelProcessedMedium.Location = new System.Drawing.Point(18, 549);
            this.labelProcessedMedium.Name = "labelProcessedMedium";
            this.labelProcessedMedium.Size = new System.Drawing.Size(0, 13);
            this.labelProcessedMedium.TabIndex = 30;
            this.labelProcessedMedium.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(19, 574);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(695, 23);
            this.progressBar.TabIndex = 29;
            this.progressBar.Visible = false;
            // 
            // buttonSaveMusics
            // 
            this.buttonSaveMusics.Location = new System.Drawing.Point(19, 605);
            this.buttonSaveMusics.Name = "buttonSaveMusics";
            this.buttonSaveMusics.Size = new System.Drawing.Size(140, 23);
            this.buttonSaveMusics.TabIndex = 28;
            this.buttonSaveMusics.Text = "Enregistrer les musiques";
            this.buttonSaveMusics.UseVisualStyleBackColor = true;
            this.buttonSaveMusics.Click += new System.EventHandler(this.OnButtonSaveMusicsClick);
            // 
            // numericUpDownYear
            // 
            this.numericUpDownYear.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSourceMedia, "Year", true));
            this.numericUpDownYear.Location = new System.Drawing.Point(65, 157);
            this.numericUpDownYear.Maximum = new decimal(new int[] {
            2100,
            0,
            0,
            0});
            this.numericUpDownYear.Name = "numericUpDownYear";
            this.numericUpDownYear.Size = new System.Drawing.Size(94, 20);
            this.numericUpDownYear.TabIndex = 27;
            this.numericUpDownYear.Value = new decimal(new int[] {
            1700,
            0,
            0,
            0});
            // 
            // bindingSourceMedia
            // 
            this.bindingSourceMedia.AllowNew = false;
            this.bindingSourceMedia.DataSource = typeof(youtube2itunes.Controllers.Medium);
            // 
            // numericUpDownNotation
            // 
            this.numericUpDownNotation.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSourceMedia, "Notation", true));
            this.numericUpDownNotation.Location = new System.Drawing.Point(210, 131);
            this.numericUpDownNotation.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownNotation.Name = "numericUpDownNotation";
            this.numericUpDownNotation.Size = new System.Drawing.Size(26, 20);
            this.numericUpDownNotation.TabIndex = 26;
            // 
            // buttonLookOnTheInternet
            // 
            this.buttonLookOnTheInternet.Location = new System.Drawing.Point(354, 382);
            this.buttonLookOnTheInternet.Name = "buttonLookOnTheInternet";
            this.buttonLookOnTheInternet.Size = new System.Drawing.Size(158, 23);
            this.buttonLookOnTheInternet.TabIndex = 25;
            this.buttonLookOnTheInternet.Text = "Rechercher les métadonnées";
            this.buttonLookOnTheInternet.UseVisualStyleBackColor = true;
            this.buttonLookOnTheInternet.Click += new System.EventHandler(this.OnButtonLookOnTheInternetClick);
            // 
            // labelNextDirectory
            // 
            this.labelNextDirectory.AutoSize = true;
            this.labelNextDirectory.Location = new System.Drawing.Point(16, 185);
            this.labelNextDirectory.Name = "labelNextDirectory";
            this.labelNextDirectory.Size = new System.Drawing.Size(87, 13);
            this.labelNextDirectory.TabIndex = 21;
            this.labelNextDirectory.Text = "Futur répertoire : ";
            this.labelNextDirectory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxNextDirectory
            // 
            this.textBoxNextDirectory.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::youtube2itunes.Properties.Settings.Default, "iTunesMusicDirectory", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxNextDirectory.Location = new System.Drawing.Point(109, 182);
            this.textBoxNextDirectory.Name = "textBoxNextDirectory";
            this.textBoxNextDirectory.ReadOnly = true;
            this.textBoxNextDirectory.Size = new System.Drawing.Size(210, 20);
            this.textBoxNextDirectory.TabIndex = 20;
            this.textBoxNextDirectory.Text = global::youtube2itunes.Properties.Settings.Default.iTunesMusicDirectory;
            // 
            // buttonCancelMusic
            // 
            this.buttonCancelMusic.Location = new System.Drawing.Point(100, 379);
            this.buttonCancelMusic.Name = "buttonCancelMusic";
            this.buttonCancelMusic.Size = new System.Drawing.Size(75, 23);
            this.buttonCancelMusic.TabIndex = 19;
            this.buttonCancelMusic.Text = "Annuler";
            this.buttonCancelMusic.UseVisualStyleBackColor = true;
            this.buttonCancelMusic.Click += new System.EventHandler(this.OnButtonCancelMusicClick);
            // 
            // buttonApplyMusic
            // 
            this.buttonApplyMusic.Location = new System.Drawing.Point(19, 379);
            this.buttonApplyMusic.Name = "buttonApplyMusic";
            this.buttonApplyMusic.Size = new System.Drawing.Size(75, 23);
            this.buttonApplyMusic.TabIndex = 18;
            this.buttonApplyMusic.Text = "Appliquer";
            this.buttonApplyMusic.UseVisualStyleBackColor = true;
            this.buttonApplyMusic.Click += new System.EventHandler(this.OnButtonApplyMusicClick);
            // 
            // labelLyrics
            // 
            this.labelLyrics.AutoSize = true;
            this.labelLyrics.Location = new System.Drawing.Point(18, 218);
            this.labelLyrics.Name = "labelLyrics";
            this.labelLyrics.Size = new System.Drawing.Size(40, 13);
            this.labelLyrics.TabIndex = 17;
            this.labelLyrics.Text = "Lyrics :";
            // 
            // textBoxLyrics
            // 
            this.textBoxLyrics.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceMedia, "Lyrics", true));
            this.textBoxLyrics.Location = new System.Drawing.Point(19, 234);
            this.textBoxLyrics.Multiline = true;
            this.textBoxLyrics.Name = "textBoxLyrics";
            this.textBoxLyrics.Size = new System.Drawing.Size(320, 139);
            this.textBoxLyrics.TabIndex = 15;
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(16, 159);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(44, 13);
            this.labelYear.TabIndex = 14;
            this.labelYear.Text = "Année :";
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.bindingSourceMedia, "Image", true));
            this.pictureBox.Location = new System.Drawing.Point(354, 16);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(360, 360);
            this.pictureBox.TabIndex = 12;
            this.pictureBox.TabStop = false;
            this.pictureBox.Click += new System.EventHandler(this.OnPictureBoxClick);
            // 
            // labelGrade
            // 
            this.labelGrade.AutoSize = true;
            this.labelGrade.Location = new System.Drawing.Point(165, 133);
            this.labelGrade.Name = "labelGrade";
            this.labelGrade.Size = new System.Drawing.Size(39, 13);
            this.labelGrade.TabIndex = 10;
            this.labelGrade.Text = "Note : ";
            // 
            // labelLength
            // 
            this.labelLength.AutoSize = true;
            this.labelLength.Location = new System.Drawing.Point(16, 133);
            this.labelLength.Name = "labelLength";
            this.labelLength.Size = new System.Drawing.Size(42, 13);
            this.labelLength.TabIndex = 9;
            this.labelLength.Text = "Durée :";
            // 
            // textBoxLength
            // 
            this.textBoxLength.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceMedia, "Duration", true));
            this.textBoxLength.Location = new System.Drawing.Point(65, 130);
            this.textBoxLength.Name = "textBoxLength";
            this.textBoxLength.ReadOnly = true;
            this.textBoxLength.Size = new System.Drawing.Size(94, 20);
            this.textBoxLength.TabIndex = 8;
            // 
            // labelGenre
            // 
            this.labelGenre.AutoSize = true;
            this.labelGenre.Location = new System.Drawing.Point(16, 107);
            this.labelGenre.Name = "labelGenre";
            this.labelGenre.Size = new System.Drawing.Size(45, 13);
            this.labelGenre.TabIndex = 7;
            this.labelGenre.Text = "Genre : ";
            // 
            // textBoxGenre
            // 
            this.textBoxGenre.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceMedia, "Genre", true));
            this.textBoxGenre.Location = new System.Drawing.Point(65, 104);
            this.textBoxGenre.Name = "textBoxGenre";
            this.textBoxGenre.Size = new System.Drawing.Size(254, 20);
            this.textBoxGenre.TabIndex = 6;
            // 
            // labelArtist
            // 
            this.labelArtist.AutoSize = true;
            this.labelArtist.Location = new System.Drawing.Point(16, 81);
            this.labelArtist.Name = "labelArtist";
            this.labelArtist.Size = new System.Drawing.Size(45, 13);
            this.labelArtist.TabIndex = 5;
            this.labelArtist.Text = "Artiste : ";
            // 
            // textBoxArtist
            // 
            this.textBoxArtist.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceMedia, "Artist", true));
            this.textBoxArtist.Location = new System.Drawing.Point(65, 78);
            this.textBoxArtist.Name = "textBoxArtist";
            this.textBoxArtist.Size = new System.Drawing.Size(254, 20);
            this.textBoxArtist.TabIndex = 4;
            // 
            // labelAlbum
            // 
            this.labelAlbum.AutoSize = true;
            this.labelAlbum.Location = new System.Drawing.Point(16, 55);
            this.labelAlbum.Name = "labelAlbum";
            this.labelAlbum.Size = new System.Drawing.Size(45, 13);
            this.labelAlbum.TabIndex = 3;
            this.labelAlbum.Text = "Album : ";
            // 
            // textBoxAlbum
            // 
            this.textBoxAlbum.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceMedia, "Album", true));
            this.textBoxAlbum.Location = new System.Drawing.Point(65, 52);
            this.textBoxAlbum.Name = "textBoxAlbum";
            this.textBoxAlbum.Size = new System.Drawing.Size(254, 20);
            this.textBoxAlbum.TabIndex = 2;
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Location = new System.Drawing.Point(16, 29);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(34, 13);
            this.labelTitle.TabIndex = 1;
            this.labelTitle.Text = "Titre :";
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceMedia, "Title", true));
            this.textBoxTitle.Location = new System.Drawing.Point(65, 26);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(254, 20);
            this.textBoxTitle.TabIndex = 0;
            // 
            // groupBoxMusics
            // 
            this.groupBoxMusics.Controls.Add(this.dataGridViewMedia);
            this.groupBoxMusics.Controls.Add(this.labelTempDirectory2Text);
            this.groupBoxMusics.Controls.Add(this.labelTempDirectory2);
            this.groupBoxMusics.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBoxMusics.Location = new System.Drawing.Point(3, 3);
            this.groupBoxMusics.Name = "groupBoxMusics";
            this.groupBoxMusics.Size = new System.Drawing.Size(274, 634);
            this.groupBoxMusics.TabIndex = 1;
            this.groupBoxMusics.TabStop = false;
            this.groupBoxMusics.Text = "Musiques téléchargées";
            // 
            // dataGridViewMedia
            // 
            this.dataGridViewMedia.AllowUserToAddRows = false;
            this.dataGridViewMedia.AllowUserToDeleteRows = false;
            this.dataGridViewMedia.AllowUserToResizeColumns = false;
            this.dataGridViewMedia.AllowUserToResizeRows = false;
            this.dataGridViewMedia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridViewMedia.AutoGenerateColumns = false;
            this.dataGridViewMedia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMedia.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumnMediumTitle});
            this.dataGridViewMedia.DataSource = this.bindingSourceMedia;
            this.dataGridViewMedia.Location = new System.Drawing.Point(9, 55);
            this.dataGridViewMedia.MultiSelect = false;
            this.dataGridViewMedia.Name = "dataGridViewMedia";
            this.dataGridViewMedia.ReadOnly = true;
            this.dataGridViewMedia.RowHeadersVisible = false;
            this.dataGridViewMedia.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewMedia.Size = new System.Drawing.Size(259, 573);
            this.dataGridViewMedia.TabIndex = 22;
            // 
            // dataGridViewTextBoxColumnMediumTitle
            // 
            this.dataGridViewTextBoxColumnMediumTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumnMediumTitle.DataPropertyName = "Title";
            this.dataGridViewTextBoxColumnMediumTitle.HeaderText = "Titre";
            this.dataGridViewTextBoxColumnMediumTitle.Name = "dataGridViewTextBoxColumnMediumTitle";
            this.dataGridViewTextBoxColumnMediumTitle.ReadOnly = true;
            // 
            // labelTempDirectory2Text
            // 
            this.labelTempDirectory2Text.AutoSize = true;
            this.labelTempDirectory2Text.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::youtube2itunes.Properties.Settings.Default, "TempDirectory", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labelTempDirectory2Text.Location = new System.Drawing.Point(6, 29);
            this.labelTempDirectory2Text.Name = "labelTempDirectory2Text";
            this.labelTempDirectory2Text.Size = new System.Drawing.Size(0, 13);
            this.labelTempDirectory2Text.TabIndex = 2;
            this.labelTempDirectory2Text.Text = global::youtube2itunes.Properties.Settings.Default.TempDirectory;
            // 
            // labelTempDirectory2
            // 
            this.labelTempDirectory2.AutoSize = true;
            this.labelTempDirectory2.Location = new System.Drawing.Point(6, 16);
            this.labelTempDirectory2.Name = "labelTempDirectory2";
            this.labelTempDirectory2.Size = new System.Drawing.Size(117, 13);
            this.labelTempDirectory2.TabIndex = 1;
            this.labelTempDirectory2.Text = "Répertoire temporaire : ";
            // 
            // tabSynchronize
            // 
            this.tabSynchronize.Controls.Add(this.groupBoxSynchronize);
            this.tabSynchronize.Location = new System.Drawing.Point(4, 22);
            this.tabSynchronize.Name = "tabSynchronize";
            this.tabSynchronize.Size = new System.Drawing.Size(1000, 640);
            this.tabSynchronize.TabIndex = 2;
            this.tabSynchronize.Text = "Synchroniser avec iTunes";
            this.tabSynchronize.UseVisualStyleBackColor = true;
            // 
            // groupBoxSynchronize
            // 
            this.groupBoxSynchronize.Controls.Add(this.buttonDetectiPod);
            this.groupBoxSynchronize.Controls.Add(this.buttonSync);
            this.groupBoxSynchronize.Controls.Add(this.buttonAddMusicToPlaylist);
            this.groupBoxSynchronize.Controls.Add(this.buttonRemoveMusicFromPlaylist);
            this.groupBoxSynchronize.Controls.Add(this.buttonDeletePlayList);
            this.groupBoxSynchronize.Controls.Add(this.buttonAddPlayList);
            this.groupBoxSynchronize.Controls.Add(this.textBoxPlayList);
            this.groupBoxSynchronize.Controls.Add(this.treeView);
            this.groupBoxSynchronize.Controls.Add(this.labelIpodConnectionStatusText);
            this.groupBoxSynchronize.Controls.Add(this.labelItunesStatusText);
            this.groupBoxSynchronize.Controls.Add(this.labelIsIpodConnected);
            this.groupBoxSynchronize.Controls.Add(this.labelIsItunesAvalaible);
            this.groupBoxSynchronize.Controls.Add(this.dataGridViewMusicsToSynchronize);
            this.groupBoxSynchronize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxSynchronize.Location = new System.Drawing.Point(0, 0);
            this.groupBoxSynchronize.Name = "groupBoxSynchronize";
            this.groupBoxSynchronize.Size = new System.Drawing.Size(1000, 640);
            this.groupBoxSynchronize.TabIndex = 3;
            this.groupBoxSynchronize.TabStop = false;
            this.groupBoxSynchronize.Text = "Musiques à synchroniser";
            // 
            // buttonDetectiPod
            // 
            this.buttonDetectiPod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDetectiPod.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDetectiPod.Location = new System.Drawing.Point(521, 118);
            this.buttonDetectiPod.Name = "buttonDetectiPod";
            this.buttonDetectiPod.Size = new System.Drawing.Size(123, 40);
            this.buttonDetectiPod.TabIndex = 47;
            this.buttonDetectiPod.Text = "Chercher iPod";
            this.buttonDetectiPod.UseVisualStyleBackColor = true;
            this.buttonDetectiPod.Click += new System.EventHandler(this.OnButtonDetectiPodClick);
            // 
            // buttonSync
            // 
            this.buttonSync.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSync.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSync.Location = new System.Drawing.Point(665, 118);
            this.buttonSync.Name = "buttonSync";
            this.buttonSync.Size = new System.Drawing.Size(123, 40);
            this.buttonSync.TabIndex = 46;
            this.buttonSync.Text = "Synchroniser";
            this.buttonSync.UseVisualStyleBackColor = true;
            this.buttonSync.Click += new System.EventHandler(this.OnButtonSyncClick);
            // 
            // buttonAddMusicToPlaylist
            // 
            this.buttonAddMusicToPlaylist.Image = global::youtube2itunes.Properties.Resources.smallforward;
            this.buttonAddMusicToPlaylist.Location = new System.Drawing.Point(485, 21);
            this.buttonAddMusicToPlaylist.Name = "buttonAddMusicToPlaylist";
            this.buttonAddMusicToPlaylist.Size = new System.Drawing.Size(24, 24);
            this.buttonAddMusicToPlaylist.TabIndex = 45;
            this.buttonAddMusicToPlaylist.UseVisualStyleBackColor = true;
            this.buttonAddMusicToPlaylist.Click += new System.EventHandler(this.OnButtonAddMusicToPlaylistClick);
            // 
            // buttonRemoveMusicFromPlaylist
            // 
            this.buttonRemoveMusicFromPlaylist.Image = global::youtube2itunes.Properties.Resources.smallback;
            this.buttonRemoveMusicFromPlaylist.Location = new System.Drawing.Point(299, 21);
            this.buttonRemoveMusicFromPlaylist.Name = "buttonRemoveMusicFromPlaylist";
            this.buttonRemoveMusicFromPlaylist.Size = new System.Drawing.Size(24, 24);
            this.buttonRemoveMusicFromPlaylist.TabIndex = 43;
            this.buttonRemoveMusicFromPlaylist.UseVisualStyleBackColor = true;
            this.buttonRemoveMusicFromPlaylist.Click += new System.EventHandler(this.OnButtonRemoveMusicFromPlaylistClick);
            // 
            // buttonDeletePlayList
            // 
            this.buttonDeletePlayList.Image = global::youtube2itunes.Properties.Resources.minus;
            this.buttonDeletePlayList.Location = new System.Drawing.Point(485, 612);
            this.buttonDeletePlayList.Name = "buttonDeletePlayList";
            this.buttonDeletePlayList.Size = new System.Drawing.Size(24, 24);
            this.buttonDeletePlayList.TabIndex = 42;
            this.buttonDeletePlayList.UseVisualStyleBackColor = true;
            this.buttonDeletePlayList.Click += new System.EventHandler(this.OnButtonDeletePlayListClick);
            // 
            // buttonAddPlayList
            // 
            this.buttonAddPlayList.Image = global::youtube2itunes.Properties.Resources.plus;
            this.buttonAddPlayList.Location = new System.Drawing.Point(455, 612);
            this.buttonAddPlayList.Name = "buttonAddPlayList";
            this.buttonAddPlayList.Size = new System.Drawing.Size(24, 24);
            this.buttonAddPlayList.TabIndex = 41;
            this.buttonAddPlayList.UseVisualStyleBackColor = true;
            this.buttonAddPlayList.Click += new System.EventHandler(this.OnButtonAddPlayListClick);
            // 
            // textBoxPlayList
            // 
            this.textBoxPlayList.Location = new System.Drawing.Point(299, 614);
            this.textBoxPlayList.Name = "textBoxPlayList";
            this.textBoxPlayList.Size = new System.Drawing.Size(150, 20);
            this.textBoxPlayList.TabIndex = 40;
            // 
            // treeView
            // 
            this.treeView.Location = new System.Drawing.Point(299, 51);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(210, 557);
            this.treeView.TabIndex = 36;
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.OnTreeViewAfterSelect);
            // 
            // labelIpodConnectionStatusText
            // 
            this.labelIpodConnectionStatusText.AutoSize = true;
            this.labelIpodConnectionStatusText.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIpodConnectionStatusText.ForeColor = System.Drawing.Color.Red;
            this.labelIpodConnectionStatusText.Location = new System.Drawing.Point(639, 69);
            this.labelIpodConnectionStatusText.Name = "labelIpodConnectionStatusText";
            this.labelIpodConnectionStatusText.Size = new System.Drawing.Size(149, 31);
            this.labelIpodConnectionStatusText.TabIndex = 35;
            this.labelIpodConnectionStatusText.Text = "Introuvable";
            // 
            // labelItunesStatusText
            // 
            this.labelItunesStatusText.AutoSize = true;
            this.labelItunesStatusText.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelItunesStatusText.ForeColor = System.Drawing.Color.Red;
            this.labelItunesStatusText.Location = new System.Drawing.Point(639, 21);
            this.labelItunesStatusText.Name = "labelItunesStatusText";
            this.labelItunesStatusText.Size = new System.Drawing.Size(92, 31);
            this.labelItunesStatusText.TabIndex = 34;
            this.labelItunesStatusText.Text = "Fermé";
            // 
            // labelIsIpodConnected
            // 
            this.labelIsIpodConnected.AutoSize = true;
            this.labelIsIpodConnected.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIsIpodConnected.Location = new System.Drawing.Point(518, 69);
            this.labelIsIpodConnected.Name = "labelIsIpodConnected";
            this.labelIsIpodConnected.Size = new System.Drawing.Size(83, 31);
            this.labelIsIpodConnected.TabIndex = 33;
            this.labelIsIpodConnected.Text = "iPod :";
            // 
            // labelIsItunesAvalaible
            // 
            this.labelIsItunesAvalaible.AutoSize = true;
            this.labelIsItunesAvalaible.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIsItunesAvalaible.Location = new System.Drawing.Point(515, 21);
            this.labelIsItunesAvalaible.Name = "labelIsItunesAvalaible";
            this.labelIsItunesAvalaible.Size = new System.Drawing.Size(118, 31);
            this.labelIsItunesAvalaible.TabIndex = 32;
            this.labelIsItunesAvalaible.Text = "iTunes : ";
            // 
            // dataGridViewMusicsToSynchronize
            // 
            this.dataGridViewMusicsToSynchronize.AllowUserToAddRows = false;
            this.dataGridViewMusicsToSynchronize.AllowUserToDeleteRows = false;
            this.dataGridViewMusicsToSynchronize.AllowUserToResizeColumns = false;
            this.dataGridViewMusicsToSynchronize.AllowUserToResizeRows = false;
            this.dataGridViewMusicsToSynchronize.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridViewMusicsToSynchronize.AutoGenerateColumns = false;
            this.dataGridViewMusicsToSynchronize.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMusicsToSynchronize.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Artist});
            this.dataGridViewMusicsToSynchronize.DataSource = this.bindingSourceMediaToSync;
            this.dataGridViewMusicsToSynchronize.Location = new System.Drawing.Point(8, 21);
            this.dataGridViewMusicsToSynchronize.Name = "dataGridViewMusicsToSynchronize";
            this.dataGridViewMusicsToSynchronize.ReadOnly = true;
            this.dataGridViewMusicsToSynchronize.RowHeadersVisible = false;
            this.dataGridViewMusicsToSynchronize.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewMusicsToSynchronize.Size = new System.Drawing.Size(285, 613);
            this.dataGridViewMusicsToSynchronize.TabIndex = 31;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Title";
            this.dataGridViewTextBoxColumn1.HeaderText = "Titre";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // Artist
            // 
            this.Artist.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Artist.DataPropertyName = "Artist";
            this.Artist.HeaderText = "Artist";
            this.Artist.Name = "Artist";
            this.Artist.ReadOnly = true;
            this.Artist.Width = 55;
            // 
            // bindingSourceMediaToSync
            // 
            this.bindingSourceMediaToSync.AllowNew = false;
            this.bindingSourceMediaToSync.DataSource = typeof(youtube2itunes.Controllers.Medium);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Images (*bmp;*.jpg;*.gif)|*.bmp;*.jpg;*.gif";
            this.openFileDialog.Title = "Sélectionner une image pour votre albm";
            this.openFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.OnOpenFileDialogFileOk);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem,
            this.toolStripMenuItemHelp});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip.TabIndex = 4;
            this.menuStrip.Text = "menuStrip1";
            // 
            // toolStripMenuItem
            // 
            this.toolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemSettings,
            this.toolStripSeparator,
            this.toolStripMenuItemQuit});
            this.toolStripMenuItem.Name = "toolStripMenuItem";
            this.toolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.toolStripMenuItem.Text = "Fichier";
            // 
            // toolStripMenuItemSettings
            // 
            this.toolStripMenuItemSettings.Name = "toolStripMenuItemSettings";
            this.toolStripMenuItemSettings.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItemSettings.Text = "Paramètres";
            this.toolStripMenuItemSettings.Click += new System.EventHandler(this.OnToolStripMenuItemSettingsClick);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(130, 6);
            // 
            // toolStripMenuItemQuit
            // 
            this.toolStripMenuItemQuit.Name = "toolStripMenuItemQuit";
            this.toolStripMenuItemQuit.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItemQuit.Text = "Quitter";
            this.toolStripMenuItemQuit.Click += new System.EventHandler(this.OnToolStripMenuItemQuitClick);
            // 
            // toolStripMenuItemHelp
            // 
            this.toolStripMenuItemHelp.Name = "toolStripMenuItemHelp";
            this.toolStripMenuItemHelp.Size = new System.Drawing.Size(43, 20);
            this.toolStripMenuItemHelp.Text = "Aide";
            this.toolStripMenuItemHelp.Click += new System.EventHandler(this.OnToolStripMenuItemHelpClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.CancelButton = this.buttonQuit;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panelMainControls);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Youtube2Itunes";
            this.Load += new System.EventHandler(this.OnMainFormLoad);
            this.panelMainControls.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabYoutube.ResumeLayout(false);
            this.panelWebBrowserControls.ResumeLayout(false);
            this.panelWebBrowserControls.PerformLayout();
            this.groupBoxDownloads.ResumeLayout(false);
            this.groupBoxDownloads.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDownloads)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDownloads)).EndInit();
            this.tabMusics.ResumeLayout(false);
            this.groupBoxMusicEdit.ResumeLayout(false);
            this.groupBoxMusicEdit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceMedia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNotation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.groupBoxMusics.ResumeLayout(false);
            this.groupBoxMusics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMedia)).EndInit();
            this.tabSynchronize.ResumeLayout(false);
            this.groupBoxSynchronize.ResumeLayout(false);
            this.groupBoxSynchronize.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMusicsToSynchronize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceMediaToSync)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panelMainControls;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabYoutube;
        private System.Windows.Forms.Panel panelWebBrowserControls;
        private System.Windows.Forms.Button buttonHome;
        private System.Windows.Forms.Button buttonDownload;
        private System.Windows.Forms.GroupBox groupBoxDownloads;
        private System.Windows.Forms.TabPage tabMusics;
        private System.Windows.Forms.TabPage tabSynchronize;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrevious;
        private System.Windows.Forms.Button buttonQuit;
        private System.Windows.Forms.Label labelTempDirectory;
        private System.Windows.Forms.Button buttonForward;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.TextBox textBoxURL;
        private System.Windows.Forms.GroupBox groupBoxMusics;
        private System.Windows.Forms.Label labelTempDirectory2Text;
        private System.Windows.Forms.Label labelTempDirectory2;
        private System.Windows.Forms.GroupBox groupBoxMusicEdit;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.Label labelLength;
        private System.Windows.Forms.TextBox textBoxLength;
        private System.Windows.Forms.Label labelGenre;
        private System.Windows.Forms.TextBox textBoxGenre;
        private System.Windows.Forms.Label labelArtist;
        private System.Windows.Forms.TextBox textBoxArtist;
        private System.Windows.Forms.Label labelAlbum;
        private System.Windows.Forms.TextBox textBoxAlbum;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.TextBox textBoxLyrics;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label labelGrade;
        private System.Windows.Forms.Label labelLyrics;
        private System.Windows.Forms.Label labelNextDirectory;
        private System.Windows.Forms.TextBox textBoxNextDirectory;
        private System.Windows.Forms.Button buttonCancelMusic;
        private System.Windows.Forms.Button buttonApplyMusic;
        private System.Windows.Forms.DataGridView dataGridViewMedia;
        private System.Windows.Forms.Button buttonLookOnTheInternet;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.BindingSource bindingSourceMedia;
        private System.Windows.Forms.BindingSource bindingSourceDownloads;
        private System.Windows.Forms.Button buttonCancelDownload;
        private System.Windows.Forms.DataGridView dataGridViewDownloads;
        private Gecko.GeckoWebBrowser webBrowser;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumnMediumTitle;
        private System.Windows.Forms.NumericUpDown numericUpDownNotation;
        private System.Windows.Forms.NumericUpDown numericUpDownYear;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumnTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumnStatus;
        private System.Windows.Forms.Button buttonSaveMusics;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label labelProcessedMedium;
        private System.Windows.Forms.Label labelTempDirectoryText;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSettings;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemQuit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemHelp;
        private System.Windows.Forms.GroupBox groupBoxSynchronize;
        private System.Windows.Forms.Label labelIsIpodConnected;
        private System.Windows.Forms.Label labelIsItunesAvalaible;
        private System.Windows.Forms.DataGridView dataGridViewMusicsToSynchronize;
        private System.Windows.Forms.Label labelItunesStatusText;
        private System.Windows.Forms.Label labelIpodConnectionStatusText;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Artist;
        private System.Windows.Forms.Button buttonAddMusicToPlaylist;
        private System.Windows.Forms.Button buttonRemoveMusicFromPlaylist;
        private System.Windows.Forms.Button buttonDeletePlayList;
        private System.Windows.Forms.Button buttonAddPlayList;
        private System.Windows.Forms.TextBox textBoxPlayList;
        private System.Windows.Forms.Button buttonSync;
        private System.Windows.Forms.Button buttonDetectiPod;
        private System.Windows.Forms.BindingSource bindingSourceMediaToSync;
    }
}