﻿using System;
using System.IO;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

using iTunesLib;
using Gecko;

using youtube2itunes.Controllers;

namespace youtube2itunes.Views
{
    public partial class MainForm : Form
    {
        private Uri _url;
        private ApiClient _apiClient;
        private iTunesClient _iTunesApp;

        public MainForm()
        {
            // Nécessaire pour que GeckoWebBrowser se charge
            Xpcom.Initialize("Firefox");

            InitializeComponent();
        }

        private void OnMainFormLoad(object sender, EventArgs e)
        {
            void CreateSourceComponents()
            {
                bindingSourceMedia.ListChanged += OnBindingSourceListChanged;
                bindingSourceDownloads.CurrentItemChanged += OnDataGridViewDownloadsSelectionChanged;

                dataGridViewMedia.AutoGenerateColumns = false;
                dataGridViewDownloads.AutoGenerateColumns = false;
            };

            // Défini l'URL de YouTube
            _url = new UriBuilder("https://www.youtube.com/").Uri;

            var allSet = CheckAndSetSettings();

            if (! allSet)
            {
                var settings = new Settings();
                settings.ShowDialog();

                if (settings.DialogResult.Equals(DialogResult.OK))
                    allSet = CheckAndSetSettings();

                if (! allSet)
                {
                    Close();
                    return;
                }
            }

            CreateSourceComponents();

            // Charge youtube
            webBrowser.Navigate(_url.ToString());
        }

        private bool CheckAndSetSettings()
        {
            if (!Directory.Exists(Properties.Settings.Default.TempDirectory))
            {
                MessageBox.Show("Répertoire temporaire invalide!", "Paramètres incorrectes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            try
            {
                _apiClient = new ApiClient(Properties.Settings.Default.ApiKey);
            }
            catch (ApiAuthenticationException exception)
            {
                MessageBox.Show(exception.Message, "Paramètres incorrectes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!Directory.Exists(Properties.Settings.Default.iTunesMusicDirectory))
            {
                MessageBox.Show("Répertoire iTunes invalide!", "Paramètres incorrectes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void OnButtonCancelClick(object sender, EventArgs e) => Close();

        private void OnButtonNextClick(object sender, EventArgs e)
        {
            if (tabControl.SelectedTab == tabSynchronize)
                Close();
            tabControl.SelectedIndex = (tabControl.SelectedIndex + 1) % tabControl.TabCount;
        }

        private void OnButtonPreviousClick(object sender, EventArgs e)
        {
            if (tabControl.SelectedTab != tabYoutube)
                tabControl.SelectedIndex = (tabControl.SelectedIndex - 1) % tabControl.TabCount;
        }

        private void _connectToiTunes()
        {
            Cursor.Current = Cursors.WaitCursor;

            if (_iTunesApp == null)
                _iTunesApp = new iTunesClient();

            labelItunesStatusText.Text = "Ouvert";
            labelItunesStatusText.ForeColor = Color.GreenYellow;
            var isiTunesStillOpen = _iTunesApp.IsStillOpen();
            if (!isiTunesStillOpen)
            {
                labelItunesStatusText.Text = "Fermé";
                labelItunesStatusText.ForeColor = Color.Red;
            }

            labelIpodConnectionStatusText.Text = "Connecté";
            labelIpodConnectionStatusText.ForeColor = Color.GreenYellow;
            buttonSync.Enabled = true;
            var isiPodFound = _iTunesApp.IsiPodConnected();
            if (!isiPodFound)
            {
                labelIpodConnectionStatusText.Text = "Introuvable";
                labelIpodConnectionStatusText.ForeColor = Color.Red;
                buttonSync.Enabled = false;
            }

            treeView.Nodes.Clear();
            var playLists = _iTunesApp.GetPlayLists();
            foreach (iTunesPlayList playList in playLists)
            {
                var treeNode = treeView.Nodes.Add(playList.Name);
                treeNode.Tag = playList;
            }

            Cursor.Current = Cursors.Default;
        }

        private void OnTabControlSelectionIndexChanged(object sender, EventArgs e)
        {
            if (tabControl.SelectedTab == tabYoutube)
                buttonPrevious.Enabled = false;
            else
                buttonPrevious.Enabled = true;

            if (tabControl.SelectedTab == tabSynchronize)
            {
                _connectToiTunes();

                buttonNext.Text = "Terminer";
            }
            else
            {
                buttonNext.Text = "Suivant";
            }

            groupBoxMusicEdit.Enabled = false;
            buttonSync.Enabled = false;

            if (bindingSourceMedia.Current != null)
            {
                groupBoxMusicEdit.Enabled = true;
                buttonSync.Enabled = true;
            }
        }

        /// <summary>
        /// Télécharge la vidéo actuelle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnButtonDownloadClick(object sender, EventArgs e)
        {
            try
            {
                // Désactive le bouton pour laisser le temps à newDownload d'être initialisé et ajouté à BindingSourceDownloads.
                // Nécessaire pour éviter ArgumentOutOfRangeException sur DataGridView
                buttonDownload.Enabled = false;

                var newDownload = new Download(webBrowser.Url, Properties.Settings.Default.TempDirectory);
                newDownload.DownloadFinished += OnDownloadFinished;

                bindingSourceDownloads.Add(newDownload);

                // Pareil que le commentaire supérieur
                buttonDownload.Enabled = true;
                try
                {
                    newDownload.Start();
                }
                catch (DownloadException error)
                {
                    MessageBox.Show(error.Message, "Erreur de téléchargement", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    var filePath = newDownload.Medium.FilePath.ToString();
                    if (File.Exists(filePath))
                        File.Delete(filePath);
                }
            }
            catch (InvalidUrlException error)
            {
                MessageBox.Show(error.Message, "Lien invalide", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Erreur inconnue", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                // Pareil que le commentaire supérieur
                buttonDownload.Enabled = true;
            }
        }

        void OnDownloadFinished(Download downloadedMusic, FinishedDownload finishedDownloadEvent)
        {
            groupBoxMusicEdit.Invoke((MethodInvoker)delegate
            {
                if (!downloadedMusic.HasFailed && !downloadedMusic.HasBeenAborted)
                    bindingSourceMedia.Add(downloadedMusic.Medium);
            });
        }

        private void OnButtonHomeClick(object sender, EventArgs e) => webBrowser.Navigate(_url.ToString());

        private void OnButtonBackClick(object sender, EventArgs e) => webBrowser.GoBack();

        private void OnButtonForwardClick(object sender, EventArgs e) => webBrowser.GoForward();

        private void OnLabelTempDirectoryTextClick(object sender, EventArgs e) => Process.Start(@labelTempDirectoryText.Text);

        private void OnButtonApplyMusicClick(object sender, EventArgs e)
        {
            var currentMedium = (Medium)bindingSourceMedia.Current;

            bindingSourceMedia.EndEdit();
        }

        private void OnButtonCancelMusicClick(object sender, EventArgs e) => bindingSourceMedia.CancelEdit();

        private void OnOpenFileDialogFileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var image = Image.FromFile(openFileDialog.FileName);
            var newRectangle = Helpers.GetScaledRectangle(image, pictureBox.ClientRectangle);
            pictureBox.Image = Helpers.GetResizedImage(image, newRectangle);
        }

        private void OnBindingSourceListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
        {
            if (bindingSourceMedia.Current == null)
                groupBoxMusicEdit.Enabled = false;
            else
                groupBoxMusicEdit.Enabled = true;
        }

        private async void OnButtonLookOnTheInternetClick(object sender, EventArgs e)
        {
            await Task.Run(() =>
            {
                var music = (Medium)bindingSourceMedia.Current;
                try
                {
                    music = _apiClient.GetMetaDataAsync(music).Result;
                    bindingSourceMedia.EndEdit();
                }
                catch (AggregateException aggregateException)
                {
                    aggregateException.Handle((exception) =>
                    {
                        if (exception is NoNewResultFoundException)
                        {
                            MessageBox.Show(exception.Message, "Aucune correspondance trouvée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return true;
                        }
                        else if (exception is ApiAuthenticationException)
                        {
                            MessageBox.Show(exception.Message, "Échec d'authentification", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return true;
                        }
                        return false; // Let anything else stop the application.
                    });
                }
            });
        }

        private void OnButtonCancelDownloadClick(object sender, EventArgs e)
        {
            var download = (Download)bindingSourceDownloads.Current;
            if (download != null)
                download.Cancel();
        }

        private void OnDataGridViewDownloadsSelectionChanged(object sender, EventArgs e)
        {
            var download = (Download)bindingSourceDownloads.Current;
            buttonCancelDownload.Invoke((MethodInvoker)delegate
            {
                buttonCancelDownload.Enabled = !download.IsFinished;
            });
        }

        // Évite d'essayer de télécharger alors que la page n'a même pas encore chargé
        private void OnWebBrowserNavigated(object sender, GeckoNavigatedEventArgs e)
        {
            buttonDownload.Enabled = true;
            textBoxURL.Text = webBrowser.Url.ToString();
        }

        private void OnPictureBoxClick(object sender, EventArgs e) => openFileDialog.ShowDialog();

        private async void OnButtonSaveMusicsClick(object sender, EventArgs e)
        {
            bindingSourceMedia.EndEdit();

            progressBar.Visible = true;
            labelProcessedMedium.Visible = true;
            Cursor.Current = Cursors.WaitCursor;
            tabControl.Enabled = false;
            panelMainControls.Enabled = false;

            var mediumCount = bindingSourceMedia.Count;
            progressBar.Maximum = mediumCount;

            try
            {
                await Task.Run(() =>
                {
                    for (var counter = 0; counter < mediumCount; counter++)
                    {
                        var processedMedium = (Medium)bindingSourceMedia[counter];

                        labelProcessedMedium.Invoke((MethodInvoker)delegate
                        {
                            labelProcessedMedium.Text = processedMedium.Title;
                        });

                        processedMedium.SaveAsMp3(Properties.Settings.Default.TempDirectory, Properties.Settings.Default.iTunesMusicDirectory);

                        progressBar.Invoke((MethodInvoker)delegate
                        {
                            progressBar.Value = counter + 1;
                        });

                        bindingSourceMediaToSync.Add(processedMedium);
                    }
                });
            } 
            catch(IOException exception)
            {
                MessageBox.Show(exception.Message, "Erreur d'export", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            labelProcessedMedium.Visible = true;
            labelProcessedMedium.Text = "Sauvegarde terminée";
            labelProcessedMedium.ForeColor = Color.LawnGreen;
            Cursor.Current = Cursors.Default;
            tabControl.Enabled = true;
            panelMainControls.Enabled = true;
        }

        private void OnDataGridViewDownloadsRowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dataGridViewDownloads.Rows[dataGridViewDownloads.RowCount - 1].Selected = true;
            bindingSourceDownloads.MoveLast();
        }

        private void OnToolStripMenuItemQuitClick(object sender, EventArgs e) => Close();

        private void OnToolStripMenuItemSettingsClick(object sender, EventArgs e)
        {
            var allDownloadFinished = true;
            foreach(Download download in bindingSourceDownloads.List)
            {
                if (!download.IsFinished)
                {
                    allDownloadFinished = false;
                    break;
                }
            }

            if (! allDownloadFinished)
            {
                MessageBox.Show(
                    "Impossible de modifier les paramètres lorsque des opérations sont en cours", 
                    "Opérations en cours...", 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Information
                );
                return;
            }

            var settings = new Settings();
            settings.ShowDialog(this);
        }

        private void OnButtonAddPlayListClick(object sender, EventArgs e)
        {
            try
            {
                var name = textBoxPlayList.Text;
                var playList = _iTunesApp.CreateNewPlayList(name);
                var treeNode = treeView.Nodes.Add(name);
                treeNode.Tag = playList;
            }
            catch (CouldNotStartiTunesException exception)
            {
                MessageBox.Show(exception.Message, "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _connectToiTunes();
            }
            catch (OperationErroriTunesException exception)
            {
                MessageBox.Show(exception.Message, "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void OnButtonDeletePlayListClick(object sender, EventArgs e)
        {
            try
            {
                var playList = (iTunesPlayList)treeView.SelectedNode.Tag;
                playList.Delete();

                treeView.Nodes.Remove(treeView.SelectedNode);
            }
            catch (CouldNotStartiTunesException exception)
            {
                MessageBox.Show(exception.Message, "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _connectToiTunes();
            }
            catch (OperationErroriTunesException exception)
            {
                MessageBox.Show(exception.Message, "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnButtonAddMusicToPlaylistClick(object sender, EventArgs e)
        {
            var selectedNode = treeView.SelectedNode;
            if (selectedNode == null)
            {
                MessageBox.Show("Sélectionnez une playlist en premier lieu", "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (selectedNode.Tag is Medium)
            {
                MessageBox.Show("Sélectionnez une playlist et pas une musique", "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var rows = dataGridViewMusicsToSynchronize.SelectedRows;
            foreach (DataGridViewRow row in rows)
            {
                var medium = (Medium)row.DataBoundItem;
                var newNode = selectedNode.Nodes.Add($"{medium.Title} - {medium.Artist}");
                newNode.Tag = medium;
            }
        }

        private void OnButtonRemoveMusicFromPlaylistClick(object sender, EventArgs e)
        {
            var selectedNode = treeView.SelectedNode;

            if (selectedNode == null)
            {
                MessageBox.Show("Sélectionnez une musique en premier lieu", "Erreur de suppression", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (selectedNode.Tag is iTunesPlayList)
            {
                MessageBox.Show("Sélectionnez une musique et et pas une playlist", "Erreur de suppression", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            treeView.Nodes.Remove(selectedNode);
        }

        private void OnButtonDetectiPodClick(object sender, EventArgs e)
        {
            _connectToiTunes();
        }

        private void OnTreeViewAfterSelect(object sender, TreeViewEventArgs e) => textBoxPlayList.Text = e.Node.Text;

        private void OnButtonSyncClick(object sender, EventArgs e)
        {
            try
            {
                if (!_iTunesApp.IsiPodConnected())
                {
                    _connectToiTunes();
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;
                tabControl.Enabled = false;

                foreach (TreeNode nodePlayList in treeView.Nodes)
                {
                    foreach(TreeNode nodeMusic in nodePlayList.Nodes)
                    {
                        var medium = (Medium)nodeMusic.Tag;
                        _iTunesApp.AddMusicToPlaylist(medium, (iTunesPlayList)nodePlayList.Tag);
                    }
                }

                _iTunesApp.SynciPod();

                Cursor.Current = Cursors.Default;
                tabControl.Enabled = true;
            }
            catch (CouldNotStartiTunesException exception)
            {
                MessageBox.Show(exception.Message, "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (OperationErrorSourceException exception)
            {
                MessageBox.Show(exception.Message, "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnToolStripMenuItemHelpClick(object sender, EventArgs e)
        {
            Help.ShowHelp(this, "help.chm");
        }
    }
}
