﻿using System;
using System.IO;
using System.ComponentModel;

using youtube2itunes.Controllers;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace youtube2itunesTests.Controllers.Tests
{
    [TestClass()]
    public class DownloadTests
    {
        private string _downloadPath;

        [TestInitialize]
        public void TestInitialize()
        {
            _downloadPath = Path.GetTempPath();
        }

        [TestMethod()]
        /// Télécharge une vidéo avec un lien invalide.
        /// Doit lever une exception
        public void DownloadVideoWithInvalidLink()
        {
            Assert.ThrowsException<InvalidUrlException>(() =>
            {
                var download = new Download(new Uri("https://www.youtube.com/"), _downloadPath);
            });
        }

        [TestMethod()]
        /// Télécharge une vidéo et l'enregistre normalement.
        public void DownloadVideo()
        {
            void OnDownloadFinished(Download downloadedMusic, FinishedDownload finishedDownloadEvent)
            {
                Assert.AreEqual("The Northern Lights - Horizons", downloadedMusic.Medium.Title);
                Assert.IsTrue(File.Exists(downloadedMusic.Medium.FilePath));
                Assert.IsFalse(downloadedMusic.HasFailed);

                var fileInfo = new FileInfo(downloadedMusic.Medium.FilePath);
                Assert.IsTrue(fileInfo.Length > 0);
            }

            try
            {
                var download = new Download(new Uri("https://www.youtube.com/watch?v=Hq8-Vs-Proc"), _downloadPath);

                download.DownloadFinished += OnDownloadFinished;
                download.Start().Wait();
            }
            catch (Exception exception)
            {
                Assert.Fail(exception.Message);
            }
        }

        [TestMethod()]
        /// Télécharge une vidéo protégée.
        /// Devrait lever une exception
        public void DownloadInexistantVideo()
        {
            var download = new Download(new Uri("https://www.youtube.com/watch?v=M34212"), _downloadPath);

            try
            {
                download.Start().Wait();
                Assert.Fail("Should have raised an exception");
            }
            catch (AggregateException exception)
            {
                var rightExceptionEncountered = false;
                exception.Handle((encounteredException) =>
                {
                    if (encounteredException is DownloadException)
                    {
                        rightExceptionEncountered = true;
                        return true;
                    }
                    return false;
                });

                Assert.IsTrue(rightExceptionEncountered);
            }
        }

        [TestMethod()]
        /// Essaie d'annuler un téléchargement
        public void CancelDownload()
        {
            void OnDownloadProgressed(object sender, PropertyChangedEventArgs e)
            {
                var download = (Download)sender;
                if (download.Status.Contains("%"))
                {
                    download.Cancel();

                    Assert.IsTrue(download.IsFinished);
                    Assert.IsFalse(download.HasFailed);
                    Assert.AreEqual("Annulé", download.Status);
                }
            }

            try
            {
                var download = new Download(new Uri("https://www.youtube.com/watch?v=Hq8-Vs-Proc"), _downloadPath);
                download.PropertyChanged += OnDownloadProgressed;

                download.Start();
            }
            catch (Exception exception)
            {
                Assert.Fail(exception.Message);
            }
        }

        [TestMethod()]
        /// Essaie d'annuler un téléchargement qui s'est déjà terminé
        /// Doit lever une exception
        public void CancelAlreadyFinishedDownload()
        {
            void OnDownloadFinished(Download downloadedMusic, FinishedDownload finishedDownloadEvent)
            {
                Assert.IsTrue(downloadedMusic.IsFinished);
                Assert.IsFalse(downloadedMusic.HasFailed);
                Assert.AreEqual("Terminé", downloadedMusic.Status);
            }

            try
            {
                var download = new Download(new Uri("https://www.youtube.com/watch?v=Hq8-Vs-Proc"), _downloadPath);
                download.DownloadFinished += OnDownloadFinished;

                download.Start().Wait();
            }
            catch (Exception exception)
            {
                Assert.Fail(exception.Message);
            }
        }
    }
}