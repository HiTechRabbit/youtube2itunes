﻿using System.IO;

using youtube2itunes.Controllers;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace youtube2itunesTests.Controllers.Tests
{
    [TestClass()]
    public class MediumTests
    {
        private Medium _medium;

        [TestInitialize]
        public void TestInitialize()
        {
            var download = new Download(new System.Uri("https://www.youtube.com/watch?v=Hq8-Vs-Proc"), Path.GetTempPath());
            download.Start().Wait();

            _medium = download.Medium;
        }

        [TestMethod]
        public void SaveMusic()
        {
            _medium.SaveAsMp3(Path.GetTempPath());

            Assert.IsTrue(File.Exists(_medium.FilePath));

            var fileInfo = new FileInfo(_medium.FilePath);
            Assert.IsTrue(fileInfo.Length > 0);

            Assert.AreEqual(fileInfo.Extension, ".mp3");

            File.Delete(_medium.FilePath);
        }
    }
}
