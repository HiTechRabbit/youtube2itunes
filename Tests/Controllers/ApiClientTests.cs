﻿using System;
using System.Threading.Tasks;

using youtube2itunes.Controllers;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace youtube2itunesTests.Controllers.Tests
{
    [TestClass]
    public class ApiClientTests
    {
        private ApiClient _client;

        [TestInitialize]
        public void TestInitialize()
        {
            _client = new ApiClient(Properties.Settings.Default.ApiKey);
        }

        [TestMethod]
        /// Essaie de récupérer les métadonnées d'une musique inexistante. Doit échouer.
        public void RetrieveInexistantMusic()
        {
            Assert.ThrowsExceptionAsync<NoNewResultFoundException>(async () =>
            {
                var music = new Medium()
                {
                    Artist = "TrevorSomething",
                    Title = "youtube2itunes"
                };

                music = await _client.GetMetaDataAsync(music);
            });
        }

        [TestMethod]
        /// Récupère les métadonnées d'une musique avec un titre et un artiste distinct.
        public async Task RetrieveKnownMusic()
        {
            try
            {
                var music = new Medium()
                {
                    Artist = "Mylène Farmer",
                    Title = "C'est une belle journée"
                };

                music = await _client.GetMetaDataAsync(music);

                Assert.IsNotNull(music);
                Assert.AreEqual(2002, music.Year);
            }
            catch (Exception exception)
            {
                Assert.Fail(exception.Message);
            }
        }

        [TestMethod]
        /// Récupère les métadonnées d'une musique seulement à partir de son titre de vidéo youtube
        public async Task RetrieveKnownMusicWithTitleOnly()
        {
            try
            {
                var music = new Medium()
                {
                    Title = "Mylène Farmer - C'est une belle journée",
                };

                music = await _client.GetMetaDataAsync(music);

                Assert.IsNotNull(music);
                Assert.AreEqual(2002, music.Year);
            }
            catch (Exception exception)
            {
                Assert.Fail(exception.Message);
            }
        }

        [TestMethod]
        /// Essaie de se connecter à l'API sans clef d'accès
        /// Doit lever une exception
        public void AccessAPIWithoutSettingKey()
        {
            var music = new Medium()
            {
                Artist = "Mylène Farmer",
                Title = "C'est une belle journée"
            };

            Assert.ThrowsExceptionAsync<ApiAuthenticationException>(async () =>
            {
                _client = new ApiClient("");
                music = await _client.GetMetaDataAsync(music);
            });
        }

        [TestMethod]
        /// Essaie de se connecter à l'API sans clef d'accès
        /// Doit lever une exception
        public void AccessAPIWithInvalidKey()
        {
            var music = new Medium()
            {
                Artist = "Mylène Farmer",
                Title = "C'est une belle journée"
            };

            _client = new ApiClient("fuckmyshitupm8");

            Assert.ThrowsExceptionAsync<ApiAuthenticationException>(async () =>
            {
                music = await _client.GetMetaDataAsync(music);
            });
        }
    }
}
